// -*- c++ -*-

// guylib library - some useful tools you might enjoy
//
// Copyright (C) 2011-2016 Guy Bensky
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.
//
// For more information, bug reports, or to contribute email me at:
// guylib@rabensky.com
//
// or you can look at the git repository at
// https://bitbucket.org/guyben/guylib

#include "fltk_updater.h"
#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_Box.H>

namespace guylib{
	namespace{
		class _fltk_updater:public time_counter::updater{
			public:
				_fltk_updater(size_t max);
				virtual size_t init(uint64_t total, const std::string &name);
				virtual void update(size_t id, uint64_t count, uint64_t total, double t);
				virtual void done(size_t id, uint64_t count, double t);
			private:
				void update_fltk();
				struct data_t{
					bool available;
					Fl_Box *text;
					Fl_Progress *progress;
				};
				size_t num;
				Fl_Window *wnd;
				std::vector<data_t> data;
		};
	}
	time_counter::updater *fltk_updater(size_t num){
		static _fltk_updater res(num);
		return &res;
	}

	namespace{
		_fltk_updater::_fltk_updater(size_t max):num(0),data(max){
			wnd=new Fl_Window(1024,20+20*data.size()*2+20);
			for (size_t i=0;i<data.size();++i){
				data[i].available=true;
				data[i].text=new Fl_Box(20,20+40*i,1024-2*20,20,"GGG");
				data[i].text->box(FL_FLAT_BOX);
				data[i].progress=new Fl_Progress(20,20+40*i+20,1024-2*20,20,"BBB");
			}
			wnd->end();
			wnd->resizable(wnd);
		}
		void _fltk_updater::update_fltk(){
			if (!wnd->shown()){
				wnd->show();
				for (size_t i=0;i<data.size();++i){
					if (data[i].available){
						data[i].text->hide();
						data[i].progress->hide();
					}
				}
			}
			while(Fl::wait(0));
		}
		size_t _fltk_updater::init(uint64_t total, const std::string &name){
			size_t id;
			for(id=0;id<data.size();++id)
				if (data[id].available)
					break;
			if (id<data.size()){
				data[id].available=false;
				data[id].text->copy_label(name.c_str());
				data[id].text->show();
				data[id].progress->copy_label(text_init(total).c_str());
				data[id].progress->minimum(0);
				data[id].progress->maximum(total);
				data[id].progress->value(0);
				data[id].progress->show();
			}
			wnd->redraw();
			update_fltk();
			return id;
		}
		void _fltk_updater::update(size_t id, uint64_t count, uint64_t total, double t){
			if (id<data.size()){
				data[id].progress->copy_label(text_update(count,total,t).c_str());
				if (total!=0){
					data[id].progress->maximum(total);
					data[id].progress->value(count);
				}
			}
			wnd->redraw();
			update_fltk();
		}
		void _fltk_updater::done(size_t id, uint64_t count, double t){
			if (id<data.size()){
				data[id].progress->copy_label(text_done(count,t).c_str());
				data[id].progress->maximum(count);
				data[id].progress->value(count);
				data[id].available=true;
			}
			update_fltk();
		}
	}
}

